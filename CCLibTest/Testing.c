#include "Testing.h"


void print_vec(CC_VECTOR *vec)
{
	int value, i;
	for (i = 0; i < VecGetCount(vec) - 1; i++)
	{
		VecGetValueByIndex(vec, i, &value);
		printf("%d, ", value);
	}
	if (VecGetCount(vec) > 0)
	{
		VecGetValueByIndex(vec, i, &value);
		printf("%d\n", value);
	}
	else
		printf("Empty vector!\n");
}

void test_cc_vector()
{
	CC_VECTOR *vec = 0;

#pragma region BasicInsertTest
	printf("VecCreate-> OUT: %d EXPECTED: 0\n", VecCreate(&vec));
	printf("VecInsertTail-> OUT: %d EXPECTED: 0\n", VecInsertTail(vec, 1));
	printf("VecInsertTail-> OUT: %d EXPECTED: 0\n", VecInsertTail(vec, 2));
	printf("VecInsertTail-> OUT: %d EXPECTED: 0\n", VecInsertTail(vec, 3));
	printf("VecInsertTail-> OUT: %d EXPECTED: 0\n", VecInsertTail(vec, 4));
	printf("VecInsertHead-> OUT: %d EXPECTED: 0\n", VecInsertHead(vec, 11));
	printf("VecInsertHead-> OUT: %d EXPECTED: 0\n", VecInsertHead(vec, 12));
	printf("VecInsertHead-> OUT: %d EXPECTED: 0\n", VecInsertHead(vec, 13));
	printf("VecInsertHead-> OUT: %d EXPECTED: 0\n", VecInsertHead(vec, 14));
	printf("Printing vector contents, expected: 14, 13, 12, 11, 1, 2, 3, 4\n");
	print_vec(vec);
#pragma endregion

#pragma region ResizeTest
	printf("VecClear-> OUT: %d EXPECTED: 0\n", VecClear(vec));
	printf("VecGetCount-> OUT: %d EXPECTED: 0\n", VecGetCount(vec));
	printf("Testing resize with %d elements\n", RESIZE_SIZE);
	for (int i = 0; i < 100; i++)
		VecInsertHead(vec, i);
	printf("Printing vector contents, expected: 99, 98, ..., 0\n");
	print_vec(vec);
#pragma endregion

#pragma region RemoveInsertSortTest
	VecClear(vec);
	printf("Building vector, should look like: 1 4 3 2\n");
	VecInsertTail(vec, 1);
	VecInsertTail(vec, 4);
	VecInsertTail(vec, 3);
	VecInsertTail(vec, 2);
	print_vec(vec);
	printf("Removing elements 4 and 3, should look like: 1 2\n");
	VecRemoveByIndex(vec, 1);
	VecRemoveByIndex(vec, 1);
	printf("Removing first and last element, vector should look like 1 and then empty\n");
	print_vec(vec);
	VecRemoveByIndex(vec, VecGetCount(vec) - 1);
	print_vec(vec);
	VecRemoveByIndex(vec, 0);
	print_vec(vec);
	printf("Building vector, should look like: 1 4 3 2\n");
	VecInsertTail(vec, 1);
	VecInsertTail(vec, 4);
	VecInsertTail(vec, 3);
	VecInsertTail(vec, 2);
	print_vec(vec);
	printf("Inserting 15 after index 0\n");
	VecInsertAfterIndex(vec, 0, 15);
	print_vec(vec);
	printf("Inserting 24 after index 4\n");
	VecInsertAfterIndex(vec, 4, 24);
	print_vec(vec);
	VecClear(vec);
	printf("Testing sort, generating unsorted array: 1 9 8 9 57 57\n");
	VecInsertTail(vec, 1);
	VecInsertTail(vec, 9);
	VecInsertTail(vec, 8);
	VecInsertTail(vec, 9);
	VecInsertHead(vec, 57);
	VecInsertHead(vec, 57);
	VecSort(vec);
	print_vec(vec);
#pragma endregion

}

void print_lst(CC_LIST_ENTRY *list)
{
	for (int i = 0; i < LstGetNodeCount(list); i++)
	{
		CC_LIST_ENTRY *aux;
		int val;
		LstGetNthNode(list, i, &aux);
		LstGetNodeValue(list, aux, &val);
		printf("%d ", val);
	}
	printf("\n");
}

void test_lst()
{
	CC_LIST_ENTRY *list, *list2;
	CC_LIST_ENTRY *aux;
	LstCreate(&list);
	LstCreate(&list2);
	LstInsertValue(list, 10);
	LstInsertValue(list, 23);
	LstInsertValue(list, 56);
	LstInsertValue(list, -12);
	LstInsertValue(list, 78);
	print_lst(list);
	printf("Removing node with index 1\n");
	LstGetNthNode(list, 1, &aux);
	LstRemoveNode(list, aux);
	print_lst(list);
	printf("Removing node with index 0\n");
	LstGetNthNode(list, 0, &aux);
	LstRemoveNode(list, aux);
	print_lst(list);
	printf("Removing last node\n");
	LstGetNthNode(list, LstGetNodeCount(list) - 1, &aux);
	LstRemoveNode(list, aux);
	print_lst(list);
	LstClear(list);
	LstInsertValue(list, 1);
	LstInsertValue(list, 4);
	LstInsertValue(list, 18);
	LstInsertValue(list, 19);
	LstInsertValue(list2, 5);
	LstInsertValue(list2, 11);
	LstInsertValue(list2, 14);
	LstInsertValue(list2, 15);
	LstInsertValue(list2, 17);
	LstMergeSortedLists(list, list2);
	print_lst(list);
	LstClear(list);
	LstInsertValue(list, 6);
	LstInsertValue(list, 2);
	LstInsertValue(list, 18);
	LstInsertValue(list, 3);
	LstInsertValue(list, 10);
	print_lst(list);
 	LstSortByValues(list);
	print_lst(list);
}

void print_htab(CC_HASH_TABLE *tab)
{
	for (int i = 0; i < HtGetKeyCount(tab); i++)
	{
		char *data = 0;
		int res = HtGetNthKey(tab, i, &data);
		printf("index %d -> %d return status expected 0 string is: %s\n", i, res, data);
	}
}

void test_htab()
{
	CC_HASH_TABLE *table;	
	int basic_enable = 1;
	if (basic_enable)
	{
#pragma region BasicTests
		printf("Creating, expected 0 %d\n", HtCreate(&table));
		printf("Adding values, expected count 5, expected keys found >= 0 for the first 5 checks\n");
		HtSetKeyValue(table, "one", 11);
		HtSetKeyValue(table, "two", 12);
		HtSetKeyValue(table, "three", 13);
		HtSetKeyValue(table, "four", 14);
		HtSetKeyValue(table, "five", 15);
		int val;

		printf("HT count: %d\n", HtGetKeyCount(table));
		HtGetKeyValue(table, "one", &val);
		printf("Get key result: %d\n", val);
		HtGetKeyValue(table, "two", &val);
		printf("Get key result: %d\n", val);
		HtGetKeyValue(table, "three", &val);
		printf("Get key result: %d\n", val);
		HtGetKeyValue(table, "four", &val);
		printf("Get key result: %d\n", val);
		HtGetKeyValue(table, "five", &val);

		printf("Get key result: %d\n", val);
		printf("Has key test: %d expected 0\n", HtHasKey(table, "three"));
		printf("Has key test: %d expected 0\n", HtHasKey(table, "five"));
		printf("Has key test: %d expected 0\n", HtHasKey(table, "four"));
		printf("Has key test: %d expected -1\n", HtHasKey(table, "NOPE"));
		
		printf("Remove key test:\n");
		printf("HT count: %d\n", HtGetKeyCount(table));
		printf("Has key test for key three: %d expected 0\n", HtHasKey(table, "three"));
		printf("Removing key three\n");
		HtRemoveKey(table, "three");
		printf("HT count: %d\n", HtGetKeyCount(table));
		printf("Has key test for key three: %d expected -1\n", HtHasKey(table, "three"));
		printf("Key overwrite test\n");
		HtGetKeyValue(table, "two", &val);
		printf("Get key result: %d\n", val);
		HtSetKeyValue(table, "two", 2112);
		HtGetKeyValue(table, "two", &val);
		printf("Get key result: %d expected 2112, len: %d\n", val, HtGetKeyCount(table));
		printf("Clear test:\n");
		HtClear(table);
		printf("Size after clear: %d\n", HtGetKeyCount(table));

#pragma endregion
	}
	printf("Creating, expected 0 %d\n", HtCreate(&table));
	printf("Adding values, expected count 5, expected keys found >= 0 for the first 5 checks\n");
	HtSetKeyValue(table, "one", 11);
	HtSetKeyValue(table, "two", 12);
	HtSetKeyValue(table, "three", 13);
	HtSetKeyValue(table, "four", 14);
	HtSetKeyValue(table, "five", 15);
	print_htab(table);
}

void print_tree_in(CC_TREE *tree)
{
	printf("Printing INORDER\n");
	for (int i = 0; i < TreeGetCount(tree); i++)
	{
		int val;
		TreeGetNthInorder(tree, i, &val);
		printf("Index: %d value->%d\n", i, val);
	}
}

void print_tree_pre(CC_TREE *tree)
{
	printf("Printing PREORDER\n");
	for (int i = 0; i < TreeGetCount(tree); i++)
	{
		int val;
		TreeGetNthPreorder(tree, i, &val);
		printf("Index: %d value->%d\n", i, val);
	}
}

void print_tree_post(CC_TREE *tree)
{
	printf("Printing POSTORDER\n");
	for (int i = 0; i < TreeGetCount(tree); i++)
	{
		int val;
		TreeGetNthPostorder(tree, i, &val);
		printf("Index: %d value->%d\n", i, val);
	}
}

void test_tree()
{
	CC_TREE *tree;
	printf("Creating expected 0 got %d\n", TreeCreate(&tree));
	printf("Buidling tree, inserting 6, 2, 18, 17, 22, 100 expected count is 6 ");
	TreeInsert(tree, 6);
	TreeInsert(tree, 2);
	TreeInsert(tree, 18);
	TreeInsert(tree, 17);
	TreeInsert(tree, 22);
	TreeInsert(tree, 100);
	printf("got %d\n", TreeGetCount(tree));
	printf("Printing tree: \n");
	print_tree_in(tree);
	print_tree_post(tree);
	print_tree_pre(tree);
	printf("Removing nodes 22, 2 and 18...\n");
	TreeRemove(tree, 18);//case two below
	TreeRemove(tree, 22);//case one below on right
	TreeRemove(tree, 2);//case leaf
	printf("Printing new tree:\n");
	print_tree_in(tree);
	TreeClear(tree);
	TreeDestroy(&tree);
}

void test_heap()
{
	CC_HEAP *heap;
	CC_VECTOR *vec;
	VecCreate(&vec);
	int val;
	HpCreateMinHeap(&heap, 0);
	HpInsert(heap, 1);
	HpInsert(heap, 3);
	HpInsert(heap, 6);
	HpInsert(heap, 6);
	HpInsert(heap, 6);
	HpInsert(heap, 5);
	HpInsert(heap, 9);
	HpInsert(heap, 8);
	printf("VEC:\n");
	HpSortToVector(heap, vec);
	print_vec(vec);
	HpInsert(heap, 1);
	HpInsert(heap, 3);
	HpInsert(heap, 6);
	HpInsert(heap, 6);
	HpInsert(heap, 6);
	HpInsert(heap, 5);
	HpInsert(heap, 9);
	HpInsert(heap, 8);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
	HpPopExtreme(heap, &val);
	print_vec(heap->heaparr);
}

int main()
{
	if (TEST_VEC)
		test_cc_vector();
	if (TEST_LST)
		test_lst();
	if (TEST_HTAB)
		test_htab();
	if (TEST_TREE)
		test_tree();
	if (TEST_HEAP)
		test_heap();
	system("pause");
	return 0;
}