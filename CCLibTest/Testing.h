#pragma once
#include <ccvector.h>
#include <cclist.h>
#include <cchashtable.h>
#include <cctree.h>
#include <ccheap.h>
#include <stdio.h>


//The size at which the vector should resize for the first time
#define RESIZE_SIZE 100
#define TEST_VEC 1
#define TEST_LST 1
#define TEST_HTAB 1
#define TEST_TREE 1
#define TEST_HEAP 1
