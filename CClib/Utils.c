#include "Utils.h"


int	ft_strcmp(const char *s1, const char *s2)
{
	int	i;

	if (!s1 || !s2)
		return -1;
	i = 0;
	while (s1[i] == s2[i])
	{
		i++;
		if (s1[i] == s2[i] && s1[i] == '\0')
			return (0);
	}
	return (s1[i] - s2[i]);
}

char	*ft_strdup(const char *src)
{
	int		i;
	char	*string;
	int		src_size;

	if (!src)
		return (char *)100;
	i = 0;
	src_size = 0;
	while (src[src_size])
		src_size++;
	string = (char*)malloc(sizeof(char) * (src_size)+1);
	while (i < src_size)
	{
		string[i] = src[i];
		i++;
	}
	string[src_size] = '\0';
	return (string);
}