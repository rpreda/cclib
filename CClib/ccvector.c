#include "ccvector.h"

static int	increaseVector(CC_VECTOR *Vector);
static void	recenterData(CC_VECTOR *Vector);

static void merge(int *arr, int l, int m, int r)
{
	int i, j, k;
	int n1 = m - l + 1;
	int n2 = r - m;
	int *L = malloc(sizeof(int) * n1);
	int	*R = malloc(sizeof(int) * n2);
	for (i = 0; i < n1; i++)
		L[i] = arr[l + i];
	for (j = 0; j < n2; j++)
		R[j] = arr[m + j + 1];
	i = 0;
	j = 0;
	k = l;
	while (i < n1 && j < n2)
	{
		if (L[i] <= R[j])
		{
			arr[k] = L[i];
			i++;
		}
		else
		{
			arr[k] = R[j];
			j++;
		}
		k++;
	}
	while (i < n1)
	{
		arr[k] = L[i];
		i++;
		k++;
	}
	while (j < n2)
	{
		arr[k] = R[j];
		j++;
		k++;
	}
	free(L);
	free(R);
}
static void mergeSort(int *arr, int l, int r)
{
	if (l < r)
	{
		int m = (l + r - 1) / 2;
		mergeSort(arr, l, m);
		mergeSort(arr, m + 1, r);
		merge(arr, l, m, r);
	}
}

int		VecCreate(CC_VECTOR **Vector)
{
	*Vector = (CC_VECTOR *)malloc(sizeof(CC_VECTOR));

	if (!*Vector)//Malloc fail
		return -2;

	(*Vector)->MemorySize = START_SIZE;
	(*Vector)->VectorMemory = (int *)malloc(START_SIZE * sizeof(int));

	if (!(*Vector)->VectorMemory)//Malloc fail
	{
		free(*Vector);
		*Vector = 0;
		return -3;
	}
	(*Vector)->BeginIndex = START_SIZE / 2;
	(*Vector)->EndIndex = START_SIZE / 2;
	return 0;
}

int		VecDestroy(CC_VECTOR **Vector)
{
	if (!*Vector)
		return -1;
	free((*Vector)->VectorMemory);
	free(*Vector);
	*Vector = 0;
	return 0;
}

//Internal library function to recenter the data in the buffer
static void	recenterData(CC_VECTOR *Vector)
{
	int i, VecSize = VecGetCount(Vector), j, aux; 
	int NewBegin = Vector->MemorySize / 2 - VecSize / 2;
	int NewEnd = Vector->MemorySize / 2 + VecSize - VecSize / 2;
	if (Vector->BeginIndex > Vector->MemorySize - Vector->EndIndex)//Move elements left
	{
		j = 0;
		for (i = NewBegin; i < NewEnd; i++)
		{
			VecGetValueByIndex(Vector, j++, &aux);
			Vector->VectorMemory[i] = aux;
		}
	}
	else
	{
		j = VecGetCount(Vector) - 1;
		for (i = NewEnd - 1; i >= NewBegin; i--)
		{
			VecGetValueByIndex(Vector, j--, &aux);
			Vector->VectorMemory[i] = aux;
		}
	}
	Vector->BeginIndex = NewBegin;
	Vector->EndIndex = NewEnd;
}

//Internal library function to increase the size of the vector
static int		increaseVector(CC_VECTOR *Vector)
{
	if (!Vector)
		return -1;
	if (Vector->MemorySize - VecGetCount(Vector) > 2 * RECENTER_TRESHOLD)
	{
		recenterData(Vector);
		return 0;
	}
	int TempBegin, TempEnd, TempMemSize, i, Value;
	TempMemSize = Vector->MemorySize * INCREASE_FACTOR;
	int *NewMemory = (int *)malloc(sizeof(int) * TempMemSize);
	if (!NewMemory)
		return -2;
	TempBegin = TempMemSize / 2 - VecGetCount(Vector) / 2;
	TempEnd = TempMemSize / 2 + VecGetCount(Vector) - VecGetCount(Vector) / 2;
	for (i = TempBegin; i < TempEnd; i++)
	{
		VecGetValueByIndex(Vector, i - TempBegin, &Value);
		NewMemory[i] = Value;
	}
	Vector->BeginIndex = TempBegin;
	Vector->EndIndex = TempEnd;
	Vector->MemorySize = TempMemSize;
	free(Vector->VectorMemory);
	Vector->VectorMemory = NewMemory;
	return 0;
}

int		VecInsertTail(CC_VECTOR *Vector, int Value)
{
	if (!Vector)
		return -1;
	if (Vector->EndIndex == Vector->MemorySize)
		if (increaseVector(Vector) == -1)
			return -1;
	Vector->VectorMemory[Vector->EndIndex++] = Value;
	return 0;
}

int		VecInsertHead(CC_VECTOR *Vector, int Value)
{
	if (!Vector)
		return -1;
	if (Vector->BeginIndex == 0)
		if (increaseVector(Vector) < 0)
			return -2;
	Vector->VectorMemory[--Vector->BeginIndex] = Value;
	return 0;
}

int		VecGetCount(CC_VECTOR *Vector)
{
	if (!Vector)
		return -1;
	return Vector->EndIndex - Vector->BeginIndex;
}

int		VecClear(CC_VECTOR *Vector)
{
	if (!Vector)
		return -1;
	Vector->BeginIndex = Vector->EndIndex = Vector->MemorySize / 2;
	return 0;
}

int		VecGetValueByIndex(CC_VECTOR *Vector, int Index, int *Value)
{
	if (!Vector)
		return -1;
	if (Index >= VecGetCount(Vector) || Index < 0)
		return -2;
	*Value = Vector->VectorMemory[Index + Vector->BeginIndex];
	return 0;
}

int		VecAlterValueAtIndex(CC_VECTOR *Vector, int Index, int Value)
{
	if (!Vector)
		return -1;
	if (Index >= VecGetCount(Vector) || Index < 0)
		return -2;
	Vector->VectorMemory[Index + Vector->BeginIndex] = Value;
	return 0;
}

int		VecGetValueUnsafe(CC_VECTOR *Vector, int Index)//Dangerous function in case of wrong range, CHECK before using it
{
	if (Index >= VecGetCount(Vector) || Index < 0)
		return 0;
	return Vector->VectorMemory[Index + Vector->BeginIndex];
}

int		VecRemoveByIndex(CC_VECTOR *Vector, int Index)
{
	if (!Vector)
		return -1;
	if (Index >= VecGetCount(Vector) || Index < 0)
		return -2;
	for (int i = Index + Vector->BeginIndex; i < Index + Vector->EndIndex - 1; i++)
		Vector->VectorMemory[i] = Vector->VectorMemory[i + 1];
	Vector->EndIndex--;
	return 0;
}

int		VecInsertAfterIndex(CC_VECTOR *Vector, int Index, int Value)
{
	int i;
	if (!Vector)
		return -1;
	if (Index >= VecGetCount(Vector) || Index < 0)//If vector empty?
		return -2;
	if (Vector->EndIndex == Vector->MemorySize)
		if (increaseVector(Vector) == -1)
			return -3;
	for (i = Vector->EndIndex + 1; i > Vector->BeginIndex + Index; i--)
		Vector->VectorMemory[i] = Vector->VectorMemory[i - 1];
	Vector->VectorMemory[i + 1] = Value;
	Vector->EndIndex++;
	return 0;
}

int		VecSort(CC_VECTOR *Vector)
{
	if (!Vector)
		return -1;
	mergeSort(Vector->VectorMemory, Vector->BeginIndex, Vector->EndIndex - 1);
	return 0;
}