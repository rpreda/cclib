#include "cchashtable.h"
#include <stdio.h>

static TABLE_DATA *GetFirstNode(CC_HASH_TABLE *HashTable, char *Key)
{
	return &(HashTable->table[Hash(Key)]);
}

static int Hash(char *Str)
{
	if (!Str)
		return 0;
	int hash = 0;
	int weigth = 1;
	while (*Str)
		hash += weigth++ * *Str++;
	return hash % TABLE_WIDTH;
}

int HtCreate(CC_HASH_TABLE **HashTable)
{
	*HashTable = malloc(sizeof(CC_HASH_TABLE));
	if (!*HashTable)
		return -1;
	for (int i = 0; i < TABLE_WIDTH; i++)
		(*HashTable)->data_count[i] = 0;
	(*HashTable)->table = malloc(sizeof(TABLE_DATA) * TABLE_WIDTH);
	if (!(*HashTable)->table)
		return -2;
	for (int i = 0; i < TABLE_WIDTH; i++)
	{
		(*HashTable)->table[i].next = 0; 
		(*HashTable)->table[i].key = 0;
	}
	(*HashTable)->element_count = 0;
	return 0;
}

void FreeList(TABLE_DATA *list)
{
	if (!list)
		return;
	FreeList(list->next);
	free(list->key);
	free(list);
}

int HtSetKeyValue(CC_HASH_TABLE *HashTable, char *Key, int Value)
{
	if (!HashTable || !Key)
		return -1;
	if (HtHasKey(HashTable, Key) >= 0)//improve if time allows
		HtRemoveKey(HashTable, Key);
	int hash = Hash(Key); 
	if (HashTable->table[hash].key == 0)
	{
		HashTable->table[hash].key = ft_strdup(Key);
		HashTable->table[hash].data = Value;
		HashTable->element_count++;
		HashTable->data_count[hash]++;
		return 0;
	}
	TABLE_DATA *list = &(HashTable->table[hash]);
	while (list->next)
		list = list->next;
	list->next = malloc(sizeof(TABLE_DATA));
	if (!list->next)
		return -2;
	list->next->next = 0;
	list->next->key = ft_strdup(Key);
	list->next->data = Value;
	HashTable->element_count++;
	HashTable->data_count[hash]++;
	return 0;
}

int HtGetKeyValue(CC_HASH_TABLE* HashTable, char* Key, int *Value)
{
	if (!HashTable || !Key)
		return -1;
	TABLE_DATA *list = GetFirstNode(HashTable, Key);
	while (list && ft_strcmp(Key, list->key))
		list = list->next;
	if (list && ft_strcmp(Key, list->key) == 0)
	{
		*Value = list->data;
		return 0;
	}
	return -1;
}

int HtDestroy(CC_HASH_TABLE **HashTable)
{
	if (!*HashTable)
		return -1;
	//Free up the lists in the table
	for (int i = 0; i < TABLE_WIDTH; i++)
		FreeList((*HashTable)->table[i].next);
	free((*HashTable)->table);
	free(*HashTable);
	*HashTable = 0;
	return 0;
}

int HtRemoveKey(CC_HASH_TABLE* HashTable, char* Key)
{
	TABLE_DATA *aux;
	if (!HashTable || !Key)
		return -1;
	TABLE_DATA *list = GetFirstNode(HashTable, Key);
	if (!ft_strcmp(list->key, Key))
	{
		if (list->next)
		{
			list->data = list->next->data;
			list->next = list->next->next;
			free(list->next);
		}
		else
		{
			free(list->key);
			list->key = 0;
		}
		HashTable->data_count[Hash(Key)]--;
		HashTable->element_count--;
		return 0;
	}
	aux = list;
	list = list->next;
	while (list->next)
	{
		if (!ft_strcmp(list->key, Key))
		{
			aux->next = list->next;
			free(list->key);
			free(list);
			HashTable->data_count[Hash(Key)]--;
			HashTable->element_count--;
			return 0;
		}
		aux = aux->next;
		list = list->next;
	}
	return -3;
}

int HtGetKeyCount(CC_HASH_TABLE* HashTable)
{
	if (!HashTable)
		return -1;
	return HashTable->element_count;
}

int HtClear(CC_HASH_TABLE* HashTable)
{
	if (!HashTable)
		return -1;
	for (int i = 0; i < TABLE_WIDTH; i++)
	{
		FreeList(HashTable->table[i].next);
		free(HashTable->table[i].key);
		HashTable->table[i].key = 0;
		HashTable->table[i].next = 0;
	}
	for (int i = 0; i < TABLE_WIDTH; i++)
		HashTable->data_count[i] = 0;
	HashTable->element_count = 0;
	return 0;
}

int HtHasKey(CC_HASH_TABLE* HashTable, char* Key)
{
	int dummyData;
	return HtGetKeyValue(HashTable, Key, &dummyData);
}

int HtGetNthKey(CC_HASH_TABLE* HashTable, int Index, char** Key)//reimplement if time allows
{
	if (!HashTable)
		return -1;
	if (Index > HashTable->element_count - 1)
		return -2;
	Index++;
	int i = 0, count = 0;
	TABLE_DATA *list = 0;
	while (count < Index)
	{
		
		list = &HashTable->table[i];
		while (list && list->key && count < Index)
		{
			count++;
			if (count < Index)
				list = list->next;
		}
		i++;
	}
	if (list)
		*Key = list->key;
	else
	{
		*Key = 0;
		return -3;
	}
	return 0;
}