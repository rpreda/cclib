#pragma once
#ifndef H_CCTREE
# include <stdlib.h>
# define H_CCTREE

typedef struct _TREE {
	int val;
	struct _TREE *left, *right;
} TREE;

typedef struct _CC_TREE {
	TREE *tree;
	int count;
} CC_TREE;
	int TreeCreate(CC_TREE **Tree);
	int TreeDestroy(CC_TREE **Tree);
	int TreeInsert(CC_TREE *Tree, int Value);
	int TreeRemove(CC_TREE *Tree, int Value);
	int TreeContains(CC_TREE *Tree, int Value);
	int TreeGetCount(CC_TREE *Tree);
	int TreeGetHeight(CC_TREE *Tree);
	int TreeClear(CC_TREE *Tree);
	int TreeGetNthPreorder(CC_TREE *Tree, int Index, int *Value);
	int TreeGetNthInorder(CC_TREE *Tree, int Index, int *Value);
	int TreeGetNthPostorder(CC_TREE *Tree, int Index, int *Value);
#endif // !H_CCTREE
