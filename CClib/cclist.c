#include "cclist.h"

static CC_LIST_DATA *CreateElement(int Value)
{
	CC_LIST_DATA *ret_val = (CC_LIST_DATA *)malloc(sizeof(CC_LIST_DATA));
	if (!ret_val)
		return 0;
	ret_val->Next = 0;
	ret_val->Data = Value;
	return ret_val;
}

static CC_LIST_ENTRY *CreateContainer(CC_LIST_DATA *Lst, int Length)
{
	CC_LIST_ENTRY *ret_val = (CC_LIST_ENTRY *)malloc(sizeof(CC_LIST_ENTRY));
	if (!ret_val)
		return 0;
	ret_val->Length = Length;
	ret_val->Lst = Lst;
	return ret_val;
}

int LstCreate(CC_LIST_ENTRY **List)
{
	*List = (CC_LIST_ENTRY *)malloc(sizeof(CC_LIST_ENTRY));
	if (!*List)
		return -1;
	(*List)->Lst = 0;
	(*List)->Length = 0;
	return 0;
}

int LstDestroy(CC_LIST_ENTRY **List)
{
	if (!*List)
		return -1;
	free(*List);
	*List = 0;
	return 0;
}

int LstInsertValue(CC_LIST_ENTRY *List, int Value)
{
	CC_LIST_DATA *element = CreateElement(Value);
	CC_LIST_DATA *temp = 0;
	if (!List)
		return -1;
	if (!element)
		return -2;
	if (!List->Lst)
		List->Lst = element;
	else
	{
		temp = List->Lst;
		while (temp->Next)
			temp = temp->Next;
		temp->Next = element;
	}
	List->Length++;
	return 0;
}

int LstRemoveNode(CC_LIST_ENTRY *List, CC_LIST_ENTRY *Node)
{
	CC_LIST_DATA *temp;
	if (!List || !Node || !List->Lst || !Node->Lst)
		return -1;
	if (List->Lst == Node->Lst)//first node needs to be removed
	{
		List->Lst = List->Lst->Next;
		List->Length--;
		return 0;
	}
	temp = List->Lst;
	while (temp->Next && temp->Next != Node->Lst)
		temp = temp->Next;
	if (!temp->Next)
		return -2;
	temp->Next = temp->Next->Next;
	List->Length--;
	return 0;
}

int LstGetNodeValue(CC_LIST_ENTRY *List, CC_LIST_ENTRY *Node, int *Value)
{
	if (!List || !Node || !Node->Lst || !List->Lst)
		return -1;
	*Value = Node->Lst->Data;
	return 0;
}

int LstGetNthNode(CC_LIST_ENTRY *List, int Index, CC_LIST_ENTRY **Node)
{
	int i = 0;
	CC_LIST_DATA *temp;
	*Node = 0;
	if (!List || !List->Lst)
		return -1;
	if (Index < 0)
		return -2;
	temp = List->Lst;
	while (temp && i != Index)
	{
		temp = temp->Next;
		i++;
	}
	if (!temp)
		return -2;
	*Node = CreateContainer(temp, List->Length - i);
	if (!Node)
		return -3;
	return 0;
}

int LstGetNodeCount(CC_LIST_ENTRY *List)
{
	if (!List)
		return -1;
	return List->Length;
}

int	LstClear(CC_LIST_ENTRY *List)
{
	CC_LIST_DATA *temp, *aux;
	if (!List || !List->Lst)
		return -1;
	temp = List->Lst;
	while (temp->Next)
	{
		aux = temp->Next;
		free(temp);
		temp = aux;
	}
	List->Length = 0;
	List->Lst = 0;
	return 0;
}

static void CopyTrailingElements(CC_LIST_DATA *dest, CC_LIST_DATA *src)
{
	while (src && dest)
	{
		dest->Next = src;
		dest = dest->Next;
		src = src->Next;
	}
}

int LstMergeSortedLists(CC_LIST_ENTRY *Destination, CC_LIST_ENTRY *Source)
{
	if (!Destination || !Source)
		return -1;
	CC_LIST_DATA *list1, *list2, *ret_val = 0, *aux = 0;
	int new_len = Destination->Length + Source->Length;
	list1 = Destination->Lst;
	list2 = Source->Lst;
	while (list1 && list2)
	{
		if (!aux)
			aux = (list1->Data > list2->Data) ? (list2) : (list1);
		else
		{
			aux->Next = (list1->Data > list2->Data) ? (list2) : (list1);
			aux = aux->Next;
		}
		if (list1->Data > list2->Data)//move pointer forward in the right list
			list2 = list2->Next;
		else
			list1 = list1->Next;
		if (!ret_val)
			ret_val = aux;
	}
	CopyTrailingElements(aux, list1);
	CopyTrailingElements(aux, list2);
	Destination->Lst = ret_val;
	Destination->Length = new_len;

	return 0;
}

static void ZeroTerminateList(CC_LIST_ENTRY *List)
{
	CC_LIST_DATA *aux;
	if (!List)
		return;
	aux = List->Lst;
	int i = 0;
	while (i < List->Length - 1)
	{
		i++;
		aux = aux->Next;
	}
	aux->Next = 0;

}

void SplitList(CC_LIST_DATA *list, CC_LIST_DATA **left, CC_LIST_DATA **right)
{
	CC_LIST_DATA *fast, *slow;
	if (!list || !list->Next)
	{
		*left = list;
		*right = 0;
		return;
	}
	slow = list;
	fast = list->Next;
	while (fast != 0)
	{
		fast = fast->Next;
		if (fast != 0)
		{
			slow = slow->Next;
			fast = fast->Next;
		}
	}
	*left = list;
	*right = slow->Next;
	slow->Next = 0;
}

int LstSortByValues(CC_LIST_ENTRY *List)
{
	if (!List)
		return -1;
	if (List->Length <= 1)
		return -1;
	CC_LIST_ENTRY *left, *right;
	LstCreate(&left);
	LstCreate(&right);
	SplitList(List->Lst, &left->Lst, &right->Lst);
	left->Length = List->Length - List->Length / 2;
	right->Length = List->Length / 2;
	LstSortByValues(left);
	LstSortByValues(right);
	LstMergeSortedLists(left, right);
	List->Lst = left->Lst;
	free(left);
	free(right);
	return 0;
}