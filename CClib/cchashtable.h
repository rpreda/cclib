#pragma once
#ifndef H_CCHASHTABLE
# define H_CCHASHTABLE
# define TABLE_WIDTH 100
#include "Utils.h"

typedef struct _TABLE_DATA {
	char *key;
	int  data;
	struct _TABLE_DATA *next;
} TABLE_DATA;

typedef struct _CC_HASH_TABLE
{
	int data_count[TABLE_WIDTH];//used for quicker iteration
	int element_count;
	TABLE_DATA *table;
} CC_HASH_TABLE;

int HtCreate(CC_HASH_TABLE** HashTable);
int HtDestroy(CC_HASH_TABLE** HashTable);
int HtSetKeyValue(CC_HASH_TABLE* HashTable, char* Key, int Value);
int HtGetKeyValue(CC_HASH_TABLE* HashTable, char* Key, int *Value);
int HtRemoveKey(CC_HASH_TABLE* HashTable, char* Key);
int HtHasKey(CC_HASH_TABLE* HashTable, char* Key);
int HtGetNthKey(CC_HASH_TABLE* HashTable, int Index, char** Key);
int HtClear(CC_HASH_TABLE* HashTable);
int HtGetKeyCount(CC_HASH_TABLE* HashTable);
static int Hash(char *Str);
#endif
