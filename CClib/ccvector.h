#pragma once
#ifndef H_CC_VECTOR
#define H_CC_VECTOR
#include <stdlib.h>
//Starting memory size, default 100 min 2
#define START_SIZE 100
//The factor used to compute the increased size OldSize * INCREASE_FACTOR default 2
#define INCREASE_FACTOR 2
//If there can be more than RECENTER_TRESHOLD free items on each side in the buffer do not realloc, instead recenter the data in increaseVector default 10
#define RECENTER_TRESHOLD 10

struct	_CC_VECTOR {
	int	*VectorMemory;
	//Size of allocated space
	int	MemorySize;
	//Data begin
	int	BeginIndex;
	//Data end
	int EndIndex;
};

typedef struct	_CC_VECTOR CC_VECTOR;


#pragma region FunctionPrototypes
	int VecCreate(CC_VECTOR **Vector);
	int VecDestroy(CC_VECTOR **Vector);
	int VecInsertTail(CC_VECTOR *Vector, int Value);
	int VecInsertHead(CC_VECTOR *Vector, int Value);
	int VecInsertAfterIndex(CC_VECTOR *Vector, int Index, int Value);
	int VecRemoveByIndex(CC_VECTOR *Vector, int Index);
	int VecGetValueByIndex(CC_VECTOR *Vector, int Index, int *Value);
	int VecGetCount(CC_VECTOR *Vector);
	int VecClear(CC_VECTOR *Vector);
	int VecSort(CC_VECTOR *Vector);
	int	VecAlterValueAtIndex(CC_VECTOR *Vector, int Index, int Value);
	int	VecGetValueUnsafe(CC_VECTOR *Vector, int Index);
#pragma endregion
#endif