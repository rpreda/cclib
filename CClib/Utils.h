#pragma once
#include <stdlib.h>
#ifndef H_UTILS
# define H_UTILS
char	*ft_strdup(const char *src);
int	ft_strcmp(const char *s1, const char *s2);
#endif
