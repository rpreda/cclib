#pragma once
#ifndef H_CC_HEAP
# define H_CC_HEAP
# include <stdlib.h>
# include "ccvector.h"

typedef struct _CC_HEAP {
	CC_VECTOR *heaparr;
	int (*GreaterThan)(int, int);
	int count;
} CC_HEAP;
int HpCreateMaxHeap(CC_HEAP **MaxHeap, CC_VECTOR* InitialElements);
int HpCreateMinHeap(CC_HEAP **MinHeap, CC_VECTOR* InitialElements);
int HpDestroy(CC_HEAP **Heap);
int HpInsert(CC_HEAP *Heap, int Value);
int HpRemove(CC_HEAP *Heap, int Value);
int HpGetExtreme(CC_HEAP *Heap, int* ExtremeValue);
int HpPopExtreme(CC_HEAP *Heap, int* ExtremeValue);
int HpGetElementCount(CC_HEAP *Heap);
int HpSortToVector(CC_HEAP *Heap, CC_VECTOR* SortedVector);
#endif // !H_CC_HEAP
