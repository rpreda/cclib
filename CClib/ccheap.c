#include "ccheap.h"

int parent(int i) {return (i - 1) / 2;}
int left(int i) {return (2 * i + 1);}
int right(int i) {return (2 * i + 2);}

int GreaterThanMax(int a, int b)
{
	return a < b;
}

int GreaterThanMin(int a, int b)
{
	return a > b;
}

void Heapify(CC_HEAP *heap, int i)
{
	int l = left(i);
	int r = right(i);
	int smallest = i;
	int len = VecGetCount(heap->heaparr);
	if ((i < 0 && i >= len))
		return;
	if (l < len && !heap->GreaterThan(VecGetValueUnsafe(heap->heaparr, l), VecGetValueUnsafe(heap->heaparr, i)))
		smallest = l;
	if (r < len && !heap->GreaterThan(VecGetValueUnsafe(heap->heaparr, r), VecGetValueUnsafe(heap->heaparr, smallest)))
		smallest = r;
	if (smallest != i)
	{
		int aux = VecGetValueUnsafe(heap->heaparr, i);
		VecAlterValueAtIndex(heap->heaparr, i, VecGetValueUnsafe(heap->heaparr, smallest));
		VecAlterValueAtIndex(heap->heaparr, smallest, aux);
		Heapify(heap, smallest);
	}
}

int AllocHeap(CC_HEAP **Heap)
{
	*Heap = malloc(sizeof(CC_HEAP));
	if (!*Heap)
		return -1;
	if (VecCreate(&(*Heap)->heaparr) != 0)
		return -2;
	(*Heap)->count = 0;
	return 0;
}

int HpCreateMinHeap(CC_HEAP **MaxHeap, CC_VECTOR* InitialElements)
{
	if (AllocHeap(MaxHeap))
		return -1;
	(*MaxHeap)->GreaterThan = GreaterThanMin;
	if (InitialElements)
		for (int i = 0; i < VecGetCount(InitialElements); i++)
			HpInsert(*MaxHeap, VecGetValueUnsafe(InitialElements, i));
	return 0;
}

int HpCreateMaxHeap(CC_HEAP **MaxHeap, CC_VECTOR* InitialElements)
{
	if (AllocHeap(MaxHeap))
		return -1;
	(*MaxHeap)->GreaterThan = GreaterThanMax;
	if (InitialElements)
		for (int i = 0; i < VecGetCount(InitialElements); i++)
			HpInsert(*MaxHeap, VecGetValueUnsafe(InitialElements, i));
	return 0;
}

int HpDestroy(CC_HEAP **Heap)
{
	if (!*Heap)
		return -1;
	VecDestroy(&(*Heap)->heaparr);
	free(*Heap);
	*Heap = 0;
	return 0;
}

int HpInsert(CC_HEAP *Heap, int Value)
{
	if (!Heap)
		return -1;
	if (Value == INT_MAX || Value == INT_MIN)
		return -3;
	int index = Heap->count;
	if (VecInsertTail(Heap->heaparr, Value) != 0)
		return -2;
	Heap->count++;
	while (index != 0 && Heap->GreaterThan(VecGetValueUnsafe(Heap->heaparr, parent(index)), VecGetValueUnsafe(Heap->heaparr, index)))
	{
		int aux = VecGetValueUnsafe(Heap->heaparr, index);
		VecAlterValueAtIndex(Heap->heaparr, index, VecGetValueUnsafe(Heap->heaparr, parent(index)));
		VecAlterValueAtIndex(Heap->heaparr, parent(index), aux);
		index = parent(index);
	}
	return 0;
}

int HpGetExtreme(CC_HEAP *Heap, int* ExtremeValue)
{
	if (!Heap)
		return -1;
	return VecGetValueByIndex(Heap->heaparr, 0, ExtremeValue);
}

int HpGetElementCount(CC_HEAP *Heap)
{
	if (!Heap)
		return -1;
	return Heap->count;
}

void AlterIndex(CC_HEAP *Heap, int i, int new_val)
{
	VecAlterValueAtIndex(Heap->heaparr, i, new_val);
	while (i != 0 && Heap->GreaterThan(VecGetValueUnsafe(Heap->heaparr, parent(i)), VecGetValueUnsafe(Heap->heaparr, i)))
	{
		int aux = VecGetValueUnsafe(Heap->heaparr, i);
		VecAlterValueAtIndex(Heap->heaparr, i, VecGetValueUnsafe(Heap->heaparr, parent(i)));
		VecAlterValueAtIndex(Heap->heaparr, parent(i), aux);
		i = parent(i);
	}
}

int HpPopExtreme(CC_HEAP *Heap, int *ExtremeValue)
{
	if (!Heap)
		return -1;
	if (Heap->count <= 0)
		return -2;
	int val = VecGetValueUnsafe(Heap->heaparr, 0);
	while (VecGetCount(Heap->heaparr) && VecGetValueUnsafe(Heap->heaparr, 0) == val)
	{
		VecAlterValueAtIndex(Heap->heaparr, 0, VecGetValueUnsafe(Heap->heaparr, VecGetCount(Heap->heaparr) - 1));
		VecRemoveByIndex(Heap->heaparr, VecGetCount(Heap->heaparr) - 1);
		Heapify(Heap, 0);
		Heap->count--;
	}
	*ExtremeValue = val;
	return 0;
}

int HpRemove(CC_HEAP *Heap, int Value)
{
	if (!Heap)
		return -1;
	int i;
	for (i = 0; i < VecGetCount(Heap->heaparr) && VecGetValueUnsafe(Heap->heaparr, i) != Value; i++);
	if (i < VecGetCount(Heap->heaparr))
	{
		if (Heap->GreaterThan == GreaterThanMin)
			AlterIndex(Heap, i, INT_MIN);
		else
			AlterIndex(Heap, i, INT_MAX);
		HpPopExtreme(Heap, &Value);
	}
	return 0;
}

int HpSortToVector(CC_HEAP *Heap, CC_VECTOR* SortedVector)
{
	if (!Heap || !SortedVector)
		return -1;
	int (*VecInsert)(CC_VECTOR *, int);
	VecInsert = (Heap->GreaterThan == GreaterThanMax) ? (VecInsertHead) : (VecInsertTail);//choose right function for insertion
	while (HpGetElementCount(Heap) > 0)
	{
		int val, i = 0, times = HpGetElementCount(Heap);
		HpPopExtreme(Heap, &val);
		times -= HpGetElementCount(Heap);
		while (i++ < times)
			VecInsert(SortedVector, val);
	}
	//add them back to heap after?
	return 0;
}