#include "cctree.h"


static void Preorder(TREE *tree, int *n, int *ret_val, int Index)
{
	if (!tree || n < 0)
		return;
	if (*n == Index)
	{
		*ret_val = tree->val;
		*n = -1;
		return;
	}
	if (*n != -1)
		(*n)++;
	Preorder(tree->left, n, ret_val, Index);
	Preorder(tree->right, n, ret_val, Index);
} 

static void Inorder(TREE *tree, int *n, int *ret_val, int Index)
{
	
	if (!tree || n < 0)
		return;
	Inorder(tree->left, n, ret_val, Index);
	if (*n == Index)
	{
		*ret_val = tree->val;
		*n = -1;
		return;
	}
	if (*n != -1)
		(*n)++;
	Inorder(tree->right, n, ret_val, Index);
}

static void Postorder(TREE *tree, int *n, int *ret_val, int Index)
{
	if (!tree || n < 0)
		return;
	Postorder(tree->left, n, ret_val, Index);
	Postorder(tree->right, n, ret_val, Index);
	if (*n == Index)
	{
		*ret_val = tree->val;
		*n = -1;
		return;
	}
	if (*n != -1)
		(*n)++;
}

static TREE *NewNode(int val)
{
	TREE *newNode = malloc(sizeof(TREE));
	if (!newNode)
		return 0;
	newNode->left = newNode->right = 0;
	newNode->val = val;
	return newNode;
}

static TREE *InsertNode(TREE *tree, int val, int *err)
{
	if (!tree)
	{
		TREE *new_node = NewNode(val);
		if (!new_node)
			*err = -1;
		return new_node;
	}
	if (val < tree->val)
		tree->left = InsertNode(tree->left, val, err);
	else if (val > tree->val)
		tree->right = InsertNode(tree->right, val, err);
	return tree;
}

static void SearchNode(TREE *tree, int val, int *found)
{
	if (!found && tree)
	{
		if (tree->val == val)
			*found = 1;
	}
	if (val < tree->val)
		SearchNode(tree->left, val, found);
	if (val > tree->val)
		SearchNode(tree->right, val, found);
}

static void FreeNodes(TREE *tree)
{
	if (tree)
	{
		FreeNodes(tree->left);
		FreeNodes(tree->right);
		free(tree);
	}
}

static TREE *FindMin(TREE *tree)
{
	while (tree->left)
		tree = tree->left;
	return tree;
}

static TREE *RemoveNode(TREE *tree, int val)
{
	if (!tree)
		return tree;
	if (val < tree->val)
		tree->left = RemoveNode(tree->left, val);
	else if (val > tree->val)
		tree->right = RemoveNode(tree->right, val);
	else
	{
		TREE *aux;
		if (!tree->left)
		{
			aux = tree->right;
			free(tree);
			return aux;
		}
		else if (!tree->right)
		{
			aux = tree->left;
			free(tree);
			return aux;
		}
		aux = FindMin(tree->right);
		tree->val = aux->val;
		tree->right = RemoveNode(tree->right, aux->val);
	}
	return tree;
}

static int GetHeight(TREE *tree)
{
	if (!tree)
		return 0;
	int left = GetHeight(tree->left);
	int right = GetHeight(tree->right);
	if (left > right)
		return left + 1;
	return right + 1;
}

int TreeCreate(CC_TREE **Tree)
{
	*Tree = malloc(sizeof(CC_TREE));
	if (!Tree)
		return -1;
	(*Tree)->count = 0;
	(*Tree)->tree = 0;
	return 0;
}

int TreeContains(CC_TREE *Tree, int Value)
{
	if (!Tree)
		return -1;
	int res = 0;
	SearchNode(Tree->tree, Value, &res);
	if (res)
		return 0;
	return -2;
}

int TreeInsert(CC_TREE *Tree, int Value)
{
	if (!Tree)
		return -1;
	int err = 0;
	Tree->tree = InsertNode(Tree->tree, Value, &err);
	if (err)
		return -1;
	Tree->count++;
	return 0;
}

int TreeRemove(CC_TREE *Tree, int Value)
{
	if (!Tree)
		return -1;
	Tree->tree = RemoveNode(Tree->tree, Value);
	Tree->count--;
	return 0;
}

int TreeGetCount(CC_TREE *Tree)
{
	if (!Tree)
		return -1;
	return Tree->count;
}

int TreeClear(CC_TREE *Tree)
{
	if (!Tree)
		return -1;
	FreeNodes(Tree->tree);
	Tree->tree = 0;
	Tree->count = 0;
	return 0;
}

int TreeDestroy(CC_TREE **Tree)
{
	if (!*Tree)
		return -1;
	TreeClear(*Tree);
	free(*Tree);
	*Tree = 0;
	return 0;
}

int TreeGetHeight(CC_TREE *Tree)
{
	if (!Tree)
		return -1;
	return GetHeight(Tree->tree);
}

int TreeGetNthPreorder(CC_TREE *Tree, int Index, int *Value)
{
	if (!Tree)
		return -1;
	if (Index < 0 || Index >= Tree->count)
		return -2;
	int n = 0;
	Preorder(Tree->tree, &n, Value, Index);
	return 0;
}


int TreeGetNthInorder(CC_TREE *Tree, int Index, int *Value)
{
	if (!Tree)
		return -1;
	if (Index < 0 || Index >= Tree->count)
		return -2;
	int n = 0;
	Inorder(Tree->tree, &n, Value, Index);
	return 0;
}

int TreeGetNthPostorder(CC_TREE *Tree, int Index, int *Value)
{
	if (!Tree)
		return -1;
	if (Index < 0 || Index >= Tree->count)
		return -2;
	int n = 0;
	Postorder(Tree->tree, &n, Value, Index);
	return 0;
}